import { ValidationChain } from 'express-validator';
import Middleware from '../middleware/Middleware';

export type RouteOptions = {
    path?: string
    validate?: Array<ValidationChain>
}

export interface RouteDefinition {
    options: RouteOptions;
    requestMethod: 'get' | 'post' | 'delete' | 'put';
    methodName: string;
    middlewares: Array<Middleware>;
}
