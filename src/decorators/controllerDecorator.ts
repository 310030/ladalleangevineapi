import 'reflect-metadata';
import ValidationMiddleware from '../middleware/ValidationMiddleware';
import { RouteDefinition, RouteOptions } from '../types/routeDefinition';

const Controller = (options?: RouteOptions): ClassDecorator => (target: any) => {
    Reflect.defineMetadata('prefix', options?.path || '', target);

    // Si le controller ne possède aucune routes, on initilize avec un tableau vide.
    if (!Reflect.hasMetadata('routes', target)) {
        Reflect.defineMetadata('routes', [], target);
    }
};

const Get = (options?: RouteOptions): MethodDecorator => (target: any, propertyKey: any): void => {
    setMetadata('get', target, propertyKey, options);
};

const Put = (options?: RouteOptions): MethodDecorator => (target: any, propertyKey: any): void => {
    setMetadata('put', target, propertyKey, options);
};

const Post = (options?: RouteOptions): MethodDecorator => (target: any, propertyKey: any): void => {
    setMetadata('post', target, propertyKey, options);
};

const Delete = (options?: RouteOptions): MethodDecorator => (target: any, propertyKey: any): void => {
    setMetadata('delete', target, propertyKey, options);
};

const setMetadata = (type: 'get' | 'put' | 'post' | 'delete', target: any, propertyKey: any, options?: RouteOptions) => {
    if (!Reflect.hasMetadata('routes', target.constructor)) {
        Reflect.defineMetadata('routes', [], target.constructor);
    }

    // Récupération des routes déjà définies pour ce controller.
    const routes = Reflect.getMetadata('routes', target.constructor) as Array<RouteDefinition>;

    const routeData: RouteDefinition = {
        requestMethod: 'get',
        options: options || {},
        methodName: propertyKey,
        middlewares: []
    };

    if (options?.validate) routeData.middlewares.push(new ValidationMiddleware());

    routes.push(routeData);
    Reflect.defineMetadata('routes', routes, target.constructor);
};

export {
    Controller,
    Get,
    Put,
    Post,
    Delete
};
