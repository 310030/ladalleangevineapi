import { NextFunction, Request, Response } from 'express';
import { validationResult } from 'express-validator';
import Middleware from './Middleware';

export default class ValidationMiddleware extends Middleware {
    protected async handle(req: Request, res: Response, next: NextFunction) {
        const errors = validationResult(req);

        if (!errors.isEmpty()) {
            res.status(422).json({ errors: errors.array() });
        } else {
            next();
        }
    }
}
