import { NextFunction, Request, Response } from 'express';

export default abstract class Middleware {
    protected abstract handle(req: Request, res: Response, next: NextFunction): any

    public handler() {
        return this.handle;
    }
}
