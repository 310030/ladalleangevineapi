import { Request, Response } from 'express';
import { Controller, Get } from '../decorators/controllerDecorator';

@Controller({
    path: '/statut'
})
export default class StateController {
    @Get({
        path: '/serveur'
    })
    public async getServerStatus(req: Request, res: Response) {
        res.send(true);
    }
}
