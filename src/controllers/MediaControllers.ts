import { Request, Response } from 'express';
import { Controller, Get } from '../decorators/controllerDecorator';
import Media from '../models/Media';

@Controller({
    path: '/medias'
})
export default class MediaController {
    @Get({
        path: '/get'
    })
    public async getMedias(req: Request, res: Response) {
        const medias = await Media.findAll();
        const data = medias.map((media) => ({
            id: media.id,
            displayUrl: media.displayUrl,
            text: media.text,
            SocialMedias: media.socialMedia
        }));

        res.json(data);
    }

    @Get({
        path: '/get/:id'
    })
    public async findMedia(req: Request, res: Response) {
        const media = await Media.findByPk(req.params.id);

        res.json({
            id: media.id,
            text: media.text,
            displayUrl: media.displayUrl,
            socialMedia: media.socialMedia
        });
    }
}
