import 'colors';
import App from './bootstrap/App';

const app = new App();

app.checkAll()
    .then(() => app.init())
    .catch((error) => {
        if (error instanceof Error) throw error;
        process.exit();
    });
