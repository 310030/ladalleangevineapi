import instagramMedias from './instagramMedias';

const commands = [
    instagramMedias.importInstagramMedias
];

commands.forEach((command) => {
    command.start();
});
