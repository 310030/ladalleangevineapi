import cron from 'node-cron';
import Instagram from 'instagram-web-api';
import instagram from '../config/instagram';
import env from '../config/env';
import Media from '../models/Media';
import SocialMedias from '../enums/SocialMedias';
import databases from '../config/databases';

const importInstagramMedias = cron.schedule('*/5 * * * *', async () => {
    databases.laDalleAngevine.authenticate().then(() => {
        const client = new Instagram({ username: env.accounts.instagram.username, password: env.accounts.instagram.password });

        client.login().then(() => {
            for (const username of instagram.accounts) {
                client.getUserByUsername({ username }).then((user) => {
                    client.getMediasById(user.id).then((medias) => {
                        console.log(medias);

                        for (const [key, value] of Object.entries(medias)) {
                            Media.findOne({
                                where: {
                                    mediaId: key,
                                    socialMedia: SocialMedias.INSTAGRAM
                                }
                            }).then((media) => {
                                if (media) {
                                    media.update({
                                        displayUrl: (value as any).displayUrl,
                                        text: (value as any).text
                                    });
                                } else {
                                    Media.create({
                                        mediaId: key,
                                        socialMedia: SocialMedias.INSTAGRAM,
                                        displayUrl: (value as any).displayUrl,
                                        text: (value as any).text
                                    });
                                }
                            });
                        }
                    });
                });
            }
        });
    });
});

export default {
    importInstagramMedias
};
