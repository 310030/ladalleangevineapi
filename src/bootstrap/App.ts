import express from 'express';
import cors from 'cors';
import fs from 'fs';
import env from '../config/env';
import controllers from '../config/controllers';
import { RouteDefinition } from '../types/routeDefinition';
import { List } from '../types/general';
import 'reflect-metadata';
import databases from '../config/databases';

export default class App {
    private _app;

    constructor() {
        this._app = express();
    }

    /**
     * Initialise les composants.
     */
    public async init() {
        this.initGlobals();
        this.initDatabases();
        this.initGlobalsMiddlewares();
        this.initControllers();
        this.listen();
    }

    /**
     * Initialise les éléments globaux.
     */
    public initGlobals() {
        this._app.use(cors());
    }

    public async initDatabases() {
        console.log('initDatabases');
        await databases.laDalleAngevine.authenticate();
        await databases.laDalleAngevine.sync();
    }

    /**
     * Initialise les middlewares.
     */
    public async initGlobalsMiddlewares() {
        this._app.use(express.json());
    }

    /**
     * Initilise les controlleurs.
     */
    public async initControllers() {
        controllers.forEach((Controller) => {
            // Instanciation du controlleur.
            const instance = new Controller();

            // Récupération du préfix spécifié dans le controlleur.
            const prefix = Reflect.getMetadata('prefix', Controller);

            // Récupération des routes du controlleur.
            const routes: Array<RouteDefinition> = Reflect.getMetadata('routes', Controller);

            routes.forEach((route) => {
                const action = (instance[route.methodName as keyof typeof instance] as Function).bind(instance);
                const middlewares = route.middlewares.map((middleware) => middleware.handler());

                this._app[route.requestMethod](prefix + route.options.path, route.options.validate || [], middlewares, action);
            });
        });
    }

    /**
     * Vérifie toutes les conditions pour lancer le serveur.
     */
    public async checkAll() {
        const check = this.checkEnv();

        return check ? Promise.resolve() : Promise.reject();
    }

    /**
     * Démarre le server
     */
    public listen() {
        this._app.listen(env.app.port, () => {
            console.log(`\n [#] Server running on port ${env.app.port}`.yellow);
        });
    }

    /**
     * Vérifie les variables d'envirronement;
     */
    public checkEnv(): boolean {
        // Variables contenant un chemin.
        const pathVariables: List<string> = {
            //
        };

        // Variables contenant un nombre.
        const numberVariables: List<any> = {
            PORT: env.app.port,
            DB_DALLE_ANGEVINE_PORT: env.databases.laDalleAngevine.port
        };

        // Variables contenant une chaîne de caractères.
        const variables: List<any> = {
            APP: env.app.env,
            // INSTAGRAM_USERNAME: env.accounts.instagram.username,
            // INSTAGRAM_PASSWORD: env.accounts.instagram.password,

            DB_DALLE_ANGEVINE_DATABASE: env.databases.laDalleAngevine.database,
            DB_DALLE_ANGEVINE_USERNAME: env.databases.laDalleAngevine.username,
            // DB_DALLE_ANGEVINE_PASSWORD: env.databases.laDalleAngevine.password,
            DB_DALLE_ANGEVINE_HOST: env.databases.laDalleAngevine.host
        };

        const invalidParameters = Object.entries(numberVariables).filter(([, value]) => {
            if (!value || !Number(value)) return true;
            return false;
        }).concat(Object.entries(variables).filter(([, value]) => {
            if (!value || !value.length) return true;
            return false;
        })).concat(Object.entries(pathVariables).filter(([, value]) => {
            if (!value || !fs.existsSync(value)) return true;
            return false;
        }));

        invalidParameters.forEach(([key]) => {
            console.log(`[!] La variable d'environnement [${key}] est invalide.`.red);
        });

        return !invalidParameters.length;
    }
}
