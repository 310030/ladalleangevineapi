import { Sequelize } from 'sequelize-typescript';
import path from 'path';
import env from './env';

const laDalleAngevine = new Sequelize({
    database: env.databases.laDalleAngevine.database,
    username: env.databases.laDalleAngevine.username,
    password: env.databases.laDalleAngevine.password,
    port: env.databases.laDalleAngevine.port,
    host: env.databases.laDalleAngevine.host,
    dialect: 'mysql',
    models: [`${path.dirname(__dirname)}/models`]
});

export default {
    laDalleAngevine
};
