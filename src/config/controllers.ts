import MediaController from '../controllers/MediaControllers';
import StateController from '../controllers/StateController';

export default [
    StateController,
    MediaController
];
