import dotenv from 'dotenv';

dotenv.config();

const str = (value: string | undefined) => value || '';
const nbr = (value: string | undefined) => parseInt(value || '0', 10);

export default {
    app: {
        env: str(process.env.APP),
        port: nbr(process.env.PORT)
    },
    accounts: {
        instagram: {
            username: str(process.env.INSTAGRAM_USERNAME),
            password: str(process.env.INSTAGRAM_PASSWORD)
        }
    },
    databases: {
        laDalleAngevine: {
            port: nbr(process.env.DB_DALLE_ANGEVINE_PORT),
            database: str(process.env.DB_DALLE_ANGEVINE_DATABASE),
            username: str(process.env.DB_DALLE_ANGEVINE_USERNAME),
            password: str(process.env.DB_DALLE_ANGEVINE_PASSWORD),
            host: str(process.env.DB_DALLE_ANGEVINE_HOST)
        }
    }
};
