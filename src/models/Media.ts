import { Optional } from 'sequelize';
import { Table, Model, Column, DataType } from 'sequelize-typescript';

interface MediaAttributes {
    id: number
    socialMedia: string,
    mediaId: string,
    displayUrl: string,
    text: string
}

interface MediaCreationAttributes extends Optional<MediaAttributes, 'id'> { }

@Table
export default class Media extends Model<MediaAttributes, MediaCreationAttributes> {
    @Column(DataType.TEXT)
    socialMedia: string;

    @Column(DataType.TEXT)
    mediaId: string;

    @Column(DataType.TEXT)
    displayUrl: string;

    @Column(DataType.TEXT)
    text: string;
}
